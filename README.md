# global

Here are global things for example all top-level-domains or something like this.

### [Top-Level-Domains (TLD)](tld)

Format:
```
com
org
net
eu
de
...
```
### [Search Engines](search_engines.csv)

Format:
```
Google;https://www.google.com/search?q=$searchString
DuckDuckGo;https://duckduckgo.com/?q=$searchString
...
```
### [Hate Words](hate)

Format:
```
stupid
idiot
...
```
